﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly MongoDbContext _dataContext;
        public PromocodesController(IRepository<PromoCode> promoCodesRepository,
                                    IRepository<Preference> preferencesRepository,
                                    IRepository<Customer> customersRepository,
                                    MongoDbContext dataContext)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _dataContext=dataContext;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodesRepository.GetAllAsync();
            var response = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(request.PreferenceId);

            if (preference == null)
                return BadRequest();

            var customers = await _customersRepository.GetWhere(d => d.Preferences.Any(x => x.PreferenceId == preference.Id));

            request.PromoCodeId = Guid.NewGuid();
            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);
            await _promoCodesRepository.AddAsync(promoCode);


            foreach (var item in promoCode.Customers)
            {
                var updateDefinition = Builders<Customer>.Update.Set(x => x.PromoCodes, new List<PromoCodeCustomer> { item });

    
                await _dataContext.Customers.UpdateOneAsync(x => x.Id == item.CustomerId, updateDefinition);
            }

            return CreatedAtAction(nameof(GetPromocodesAsync), new { }, null);
        }
    }
}