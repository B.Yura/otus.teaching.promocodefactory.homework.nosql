﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly MongoDbContext _dataContext;

        public MongoRepository(MongoDbContext mongoDbContext)
        {
            _dataContext=mongoDbContext;
        }

        public async Task AddAsync(T entity)
        {
            await _dataContext.Set<T>().InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _dataContext.Set<T>().DeleteOneAsync(x => x.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().Find(_ => true).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<T>().Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public Task<T> GetFirstWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();

        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<T>> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _dataContext.Set<T>().Find(predicate).ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            await _dataContext.Set<T>().ReplaceOneAsync(x => x.Id == entity.Id, entity);
        }
    }
}
