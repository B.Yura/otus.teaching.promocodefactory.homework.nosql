﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;


namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer : IDbInitializer
    {
        private readonly MongoDbContext _dataContext;
        public MongoDbInitializer(MongoDbContext dataContext)
        {
            _dataContext=dataContext;
        }
        public void InitializeDb()
        {

            if (!_dataContext.Customers.Find(x => FakeDataFactory.Customers.Select(x => x.Id).Contains(x.Id)).Any())
                _dataContext.Customers.InsertMany(FakeDataFactory.Customers);

            if (!_dataContext.Preferences.Find(x => FakeDataFactory.Preferences.Select(x => x.Id).Contains(x.Id)).Any())
                _dataContext.Preferences.InsertMany(FakeDataFactory.Preferences);

        }
    }
}
