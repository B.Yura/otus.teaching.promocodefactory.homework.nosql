﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core
{
    public class MongoDbContext
    {
        public readonly IMongoCollection<Customer> Customers;
        public readonly IMongoCollection<Preference> Preferences;
        public readonly IMongoCollection<PromoCode> PromoCodes;
        private readonly IMongoDatabase mongoDatabase;
        public MongoDbContext(IOptionsSnapshot<MongoDbSettings> options)
        {
            var mongoClient = new MongoClient(options.Value.ConnectionString);
            mongoDatabase = mongoClient.GetDatabase(options.Value.DatabaseName);
            Customers = mongoDatabase.GetCollection<Customer>(options.Value.Customer);
            Preferences= mongoDatabase.GetCollection<Preference>(options.Value.Preference);
            PromoCodes = mongoDatabase.GetCollection<PromoCode>(options.Value.PromoCode);
        }
        public IMongoCollection<T> Set<T>() where T : class
        {
            return mongoDatabase.GetCollection<T>(typeof(T).Name);
        }
    }
}
