﻿namespace Otus.Teaching.Pcf.GivingToCustomer.Core
{
    public class MongoDbSettings
    {
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;
        public string Customer { get; set; } = null!;
        public string Preference { get; set; } = null!;
        public string PromoCode { get; set; } = null!;
    }
}
