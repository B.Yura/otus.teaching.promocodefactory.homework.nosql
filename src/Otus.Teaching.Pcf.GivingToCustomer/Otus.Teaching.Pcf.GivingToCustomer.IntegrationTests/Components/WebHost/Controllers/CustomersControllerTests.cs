﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Moq;
using Otus.Teaching.Pcf.GivingToCustomer.Core;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Xunit;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Components.WebHost.Controllers
{
    [Collection(EfDatabaseCollection.DbCollection)]
    public class CustomersControllerTests : IClassFixture<EfDatabaseFixture>
    {
        private readonly CustomersController _customersController;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        public CustomersControllerTests(EfDatabaseFixture efDatabaseFixture)
        {
            var options = new MongoDbSettings
            {
                ConnectionString ="mongodb://localhost:27017/GivingToCustomer_db",
                Customer= "Customer",
                DatabaseName= "GivingToCustomer_db",
                Preference= "Preference",
                PromoCode = "PromoCode"

            };
            var mock = new Mock<IOptionsSnapshot<MongoDbSettings>>();
            mock.Setup(m => m.Value).Returns(options);
            var _mongoDbContext = new MongoDbContext(mock.Object);

            _customerRepository = new MongoRepository<Customer>(_mongoDbContext);
            _preferenceRepository = new MongoRepository<Preference>(_mongoDbContext);
            _promocodeRepository = new MongoRepository<PromoCode>(_mongoDbContext);

            _customersController = new CustomersController(
                    _customerRepository,
                    _preferenceRepository,
                    _promocodeRepository);
        }

        [Fact]
        public async Task CreateCustomerAsync_CanCreateCustomer_ShouldCreateExpectedCustomer()
        {
            //Arrange 
            var preferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c");
            var request = new CreateOrEditCustomerRequest()
            {
                Email = "some@mail.ru",
                FirstName = "Иван",
                LastName = "Петров",
                PreferenceIds = new List<Guid>()
                {
                    preferenceId
                }
            };

            //Act
            var result = await _customersController.CreateCustomerAsync(request);
            var actionResult = result.Result as CreatedAtActionResult;
            var id = (Guid)actionResult.Value;

            //Assert
            var actual = await _customerRepository.GetByIdAsync(id);


            actual.Email.Should().Be(request.Email);
            actual.FirstName.Should().Be(request.FirstName);
            actual.LastName.Should().Be(request.LastName);
            actual.Preferences.Should()
                .ContainSingle()
                .And
                .Contain(x => x.PreferenceId == preferenceId);
        }
    }
}